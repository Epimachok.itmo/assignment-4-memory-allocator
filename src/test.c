#include "mem.h"
#include "mem_internals.h"
#define HEAP_INIT_SIZE 4096

void print_test_title(int testNum, char *title) {
    printf("\n === Test %d: %s ===\n", testNum, title);
}

void test_1() {
    print_test_title(1, "Simple");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL){
        printf("Error heap_init\n");
        return;
    }
    _malloc(REGION_MIN_SIZE/10);
    debug_heap(stdout, heap);
}

void test_2() {
    print_test_title(2, "free block");
    void* heap = heap_init(REGION_MIN_SIZE);
    _malloc(REGION_MIN_SIZE/4);
    void* block = _malloc(REGION_MIN_SIZE/4);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _free(block);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_3() {
    print_test_title(3, "free two blocks");
    void* heap = heap_init(REGION_MIN_SIZE);
    _malloc(REGION_MIN_SIZE/8);
    void* block_1 = _malloc(REGION_MIN_SIZE/8);
    _malloc(REGION_MIN_SIZE/8);
    void* block_2 = _malloc(REGION_MIN_SIZE/8);
    _malloc(REGION_MIN_SIZE/8);
    printf("Before free:\n");
    debug_heap(stdout, heap);
    _free(block_1);
    _free(block_2);
    printf("\nAfter free:\n");
    debug_heap(stdout, heap);
}

void test_4() {
    print_test_title(4, "overflow");
    void* heap = heap_init(REGION_MIN_SIZE);
    _malloc(REGION_MIN_SIZE/2);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _malloc(REGION_MIN_SIZE);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_5() {
    print_test_title(5, "overflow");
    void* heap = heap_init(0);
    heap_init(REGION_MIN_SIZE);
    _malloc(REGION_MIN_SIZE/2);
    _malloc(REGION_MIN_SIZE/3);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _malloc(REGION_MIN_SIZE);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_all() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}
